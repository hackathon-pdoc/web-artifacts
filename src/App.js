import React, {Component} from 'react';
import {
    withRouter,
    Link
} from 'react-router-dom';

import IconMenu from 'material-ui/IconMenu';
import IconButton from 'material-ui/IconButton';
import FontIcon from 'material-ui/FontIcon';
import NavigationExpandMoreIcon from 'material-ui/svg-icons/navigation/expand-more';
import MenuItem from 'material-ui/MenuItem';
import DropDownMenu from 'material-ui/DropDownMenu';
import RaisedButton from 'material-ui/RaisedButton';
import {Toolbar, ToolbarGroup, ToolbarSeparator, ToolbarTitle} from 'material-ui/Toolbar';

import getMuiTheme from 'material-ui/styles/getMuiTheme'





import {
    Nav,
    NavItem,
    Navbar
} from 'react-bootstrap';
import AWS from 'aws-sdk';
import {CognitoUserPool,} from 'amazon-cognito-identity-js';





import Routes from './Routes';
import RouteNavItem from './components/RouteNavItem';
import config from './config.js';
import SearchBar from './containers/modules/searchbar'
import './App.css';
import { invokeProfile } from './libs/awsLib';
import Avatar from 'material-ui/Avatar';
import Chip from 'material-ui/Chip';
import SvgIconFace from 'material-ui/svg-icons/action/face';

const styles = {
    chip: {
        margin: 4,
    },
    wrapper: {
        display: 'flex',
        flexWrap: 'wrap',
    },
};


class App extends Component {
    user = null;
    userProfile = null;
    static childContextTypes = {
        muiTheme: React.PropTypes.object
    }
    getChildContext() {
        return {
            muiTheme: getMuiTheme()
        }
    }
    constructor(props) {
        super(props);

        this.state = {
            userToken: null,
            isLoadingUserToken: true,
            userProfile: {},
            value: 3,
            navClass: ''
        };
        this.currentUser = null;
        this.updateUserToken = this.updateUserToken.bind(this);
    }
    handleChange = (event, index, value) => this.setState({value});

    componentWillUnmount() {
        window.removeEventListener('scroll', this.handleScroll);
    }

    handleScroll(event) {
        let scrollTop = event.srcElement.body.scrollTop;
            this.setState({
                navClass: (scrollTop>36) ? 'fixed-header' : ""
            })
    }
    async componentDidMount() {
        const currentUser = await this.getCurrentUser();
        window.addEventListener('scroll', this.handleScroll.bind(this));

        if (currentUser === null) {
            this.setState({isLoadingUserToken: false});
            return;
        }

        try {
            const userToken = await this.getUserToken(currentUser);
            this.updateUserToken(userToken);

        }
        catch (e) {
            try {
                AWS.config.credentials.clearCachedId();
            } catch(e) {
                return;
            }
            this.props.history.push('/login');
        }
        this.setState({isLoadingUserToken: true});


    }

    updateUserToken(userToken)  {
        this.setState({
            userToken: userToken
        });
    }

    async getProfile(_id) {
        let data =  await invokeProfile({path: '/profile/'+_id}, this.state.userToken);
        console.log(data);
        this.setState({
            userProfile: {
                ...this.state.userProfile,
                ...data,
            },
            isLoadingUserToken: false
        });

    }

    async getCurrentUser() {
        var _this = this;

        const userPool = new CognitoUserPool({
            UserPoolId: config.cognito.USER_POOL_ID,
            ClientId: config.cognito.APP_CLIENT_ID
        });
        if (this.userProfile === null) this.userProfile = {};

        var cognitoUser = userPool.getCurrentUser();

        if (cognitoUser) {
            cognitoUser.getSession((err, session) => {
                if (err) {
                    // this.logoutUser();
                    return;
                }
                this.user = cognitoUser;
            });
            cognitoUser.getUserAttributes(function (err, result) {
                if (err) {
                    return;
                }
                for (let i = 0; i < result.length; i++) {
                    let key = result[i].getName() === "sub" ? "_id" : result[i].getName();
                    _this.userProfile[key] = result[i].getValue();
                }
                _this.setState({
                    userProfile: _this.userProfile
                })
            });
        }
        this.currentUser = cognitoUser;
        return cognitoUser;
    }

    setUsersAttribute() {
        var cognitoUser = this.user;
        return cognitoUser;
    }

    getUserToken(currentUser) {
        return new Promise((resolve, reject) => {
            currentUser.getSession(function (err, session) {
                if (err) {
                    reject(err);
                    return;
                }
                resolve(session.getIdToken().getJwtToken());
            });
        });
    }

    handleNavLink = (event) => {
        event.preventDefault();
        this.props.history.push(event.currentTarget.getAttribute('href'));
    }

    handleLogout = (event) => {
        const currentUser = this.currentUser;

        if (currentUser !== null) {
            currentUser.signOut();
        }

        if (AWS.config.credentials) {
            AWS.config.credentials.clearCachedId();
        }

        this.updateUserToken(null);

        this.props.history.push('/login');
    }

    render() {
        const childProps = {
            userToken: this.state.userToken,
            user: this.user,
            updateUserToken: this.updateUserToken,
            profile: this.state.userProfile,
            config: config,
            url: window.location.href.split(window.location.pathname)[0]
        };
        if(this.state.userProfile._id && this.state.isLoadingUserToken) {
            this.getProfile(this.state.userProfile._id)

        }
        return !this.state.isLoadingUserToken
            &&
            (
            <div className="App">
                <Toolbar>
                    <ToolbarGroup firstChild={true}>
                        <img
                            onClick={this.handleNavLink}
                            href="/"
                            src="/img/iGnite.png"
                            className="App-logo"
                            width={35}
                        />

                    </ToolbarGroup>
                    <ToolbarGroup>
                        <Chip
                            style={styles.chip}
                        >
                            <Avatar  color="#444" icon={<SvgIconFace />} />
                            {this.state.userProfile.first_name}
                        </Chip>
                        <ToolbarSeparator />
                        <RaisedButton
                            label="Ignite now!"
                            onClick={this.handleNavLink}
                            href="/post/create"
                            primary={true} />
                        <IconMenu
                            iconButtonElement={
                                <IconButton touch={true}>
                                    <NavigationExpandMoreIcon />
                                </IconButton>
                            }
                        >

                            {this.state.userToken

                                ? [<MenuItem key={1} onClick={this.handleNavLink}
                                                 href="/profile">Profile</MenuItem>,
                                    <MenuItem key={3} onClick={this.handleLogout}>Logout</MenuItem>,
                                ]
                                : [<MenuItem key={1} onClick={this.handleNavLink}
                                                 href="/signup">Signup</MenuItem>,
                                    <MenuItem key={2} onClick={this.handleNavLink}
                                                  href="/login">Login</MenuItem>]}
                        </IconMenu>
                    </ToolbarGroup>
                </Toolbar>
                <div className="container">
                    <Routes user={this.user}  childProps={childProps}/>
                </div>
            </div>
            );
    }

}

export default withRouter(App);
