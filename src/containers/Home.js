import React, { Component } from 'react';
import { withRouter, Link } from 'react-router-dom';
import {
  PageHeader,

} from 'react-bootstrap';
import './Home.css';
import { invokeArticleAPI} from '../libs/awsLib'

class Home extends Component {

  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
        articles: [],
    };
  }
  async articles() {
      let data = await invokeArticleAPI({method: "GET", body:{}}, this.props.userToken);
      console.debug(data);
      return data;
  }
  async componentDidMount() {
    if (this.props.userToken === null) {
      return;
    }

    this.setState({ isLoading: true });

    try {
      const results = await this.articles();
      this.setState({ articles: results });
    }
    catch(e) {
      // alert(e);
    }

    this.setState({ isLoading: false });
  }

  renderLander() {
    return (
      <div className="lander">
        <h1>Scratch</h1>
        <p>A simple note taking app</p>
        <div>
          <Link to="/login" className="btn btn-info btn-lg">Login</Link>
          <Link to="/signup" className="btn btn-success btn-lg">Signup</Link>
        </div>
      </div>
    );
  }

  renderNotes() {
    return (
      <div className="notes">
        <PageHeader>Your Notes</PageHeader>

      </div>
    );
  }

  render() {
    return (
      <div className="Home">
        { this.props.userToken === null
          ? this.renderLander()
          : this.renderNotes() }
      </div>
    );
  }
}

export default withRouter(Home);
