import React, {Component} from 'react';
import {
    TextField,
    SelectField,
    MenuItem,
    FlatButton,
    DatePicker,
    Card,
    RaisedButton,
    TimePicker
} from 'material-ui';
import { withRouter } from 'react-router-dom';
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import Dropzone from 'react-dropzone'
import {s3Upload, invokeArticleAPI} from '../../libs/awsLib'
import config from '../../config'
import { Editor } from 'react-draft-wysiwyg';
import '../../../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import './CreatePost.css';
import "react-image-gallery/styles/css/image-gallery.css";
import ImageGallery from 'react-image-gallery';
import draftToHtml from 'draftjs-to-html';
import moment from 'moment'

class CreatePost extends Component {
    images = [];
    constructor(props) {
        super(props);
        this.state = {
            editorState: "",
            value: "",
            count: 0,
            type: 0,
            target:0,
            event_created: "",
            title: "",
            description: "",
            banner:"",
            location: null,
            isUploading: false,
            time_start: 0,
            time_end: 0,
            recipient: ""

        };
        this.onDrop = this.onDrop.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.onEditorStateChange = this.onEditorStateChange.bind(this);
        this.handleDateChange = this.handleDateChange.bind(this)
        this.handleTimeStart = this.handleTimeStart.bind(this)
        this.handleTimeEnd = this.handleTimeEnd.bind(this)
        this.handleRecipientChange = this.handleRecipientChange.bind(this)
        this.handleTargetChange = this.handleTargetChange.bind(this)
    }
    share(id){
        let url = 'https://asurion.facebook.com/sharer.php?display=popup&u=' +"https://rve2w7n4qb.execute-api.us-east-2.amazonaws.com/prod/share-article/"+id;
        let options = 'toolbar=0,status=0,resizable=1,width=626,height=436';
        window.open(url,'sharer',options);
    }
    async handleSubmit() {
        let payload = {
            title: this.state.title,
            description: this.state.description,
            banner: this.state.banner,
            location: this.state.location,
            date_created: moment().toISOString(),
            expire_date: moment().add(30, 'days').format(moment().ISO_8601),
            event_date: this.state.event_created,
            type: this.state.type,
            target: this.state.target,
        };

        if(payload.type==1) {
            payload = {
                ...payload,
                time_start: this.state.time_start,
                time_end: this.state.time_end,
                recipient: this.state.recipient
            }
        }
        let data = await invokeArticleAPI({method: "PUT", body:payload}, this.props.userToken);

        this.share(data.insertId);
        if(data.insertId===200) this.props.history.push('/article/'+data.insertId);

    }

    /**
     * Handles
     */
    handleChange (event, index, value) {
        this.setState({type:value});
    }
    handleTargetChange (event, index, value) {
        this.setState({target:value});
    }
    handleRecipientChange (event, value) {
        console.log(arguments);
        this.setState({recipient:value});
    }
    handleTextChange (event, value) {
        this.setState({title:value});
    }
    onEditorStateChange(data) {
        this.setState({
            description:draftToHtml(data)
        });
    }
    handleDateChange(event, value) {
        this.setState({
            event_created: value
        })
    }

    handleTimeStart(event, value) {
        this.setState({
            time_start:value
        });
    }

    handleTimeEnd(event, value) {
        this.setState({
            time_end:value
        });
    }






    findWithAttr(array, attr, value) {
        for(var i = 0; i < array.length; i += 1) {
            console.debug(value);
            console.debug(array[i][attr]);

            if(value.indexOf(encodeURI(array[i][attr])) > -1 ) {
                return i;
            }
        }
        return -1;
    }
    uploadFile = async (file) =>  {
        if (file && file.size > config.MAX_ATTACHMENT_SIZE) {
            alert('Please pick a file smaller than 5MB');
            return;
        }
        this.images.push({
            fileName: file.name,
            original: file.preview,
            thumbnail:file.preview,
            thumbnailClass: "loading",
            originalClass: "loading"
        });
        this.setState({ isUploading: this.images.length });
        try {
            let data = await s3Upload(file, this.props.userToken);

            let addBanner = [...this.state.banner];
            let tempImage = [...this.images];
            let index = this.findWithAttr(this.images, "fileName", data.Location);
            this.images[index] = {
                original: data.Location,
                thumbnail:data.Location,
                thumbnailClass: "",
                originalClass: ""
            };

            addBanner.push(data);

            this.setState({
                banner:addBanner,
                images: tempImage
            });
            console.debug(data);

        }
        catch(e) {
            alert(e);
            // this.setState({ isUploading: false });
        }
    }
    static childContextTypes = {
        muiTheme: React.PropTypes.object
    }
    onDrop(acceptedFiles, rejectedFiles) {
        acceptedFiles.forEach((v, i)=> {
            // s3Upload(v,)
            this.uploadFile(v)
        });
    }
    getImages() {
        if(this.images.length!==0) {
            return (
                <ImageGallery
                    items={this.images}
                    slideInterval={2000}

                />
            );
        }
        return null;
    }
    getChildContext() {
        return {
            muiTheme: getMuiTheme()
        }
    }
    render() {
        let date = "Target Date",
            targetValue = "Amount",
            showLocation = 'hidden';

        switch (this.state.type) {
            case 1:
                date = "Event Date";
                targetValue = "Volunteers";
                showLocation = '';
                break;
            case 2:
                date = "Until";
                targetValue = "Signatures";
                break;
            default:
                date = "Target Date"
                targetValue = "Amount";
                showLocation = 'hidden';

        }

        return (
            <div id='create-post-container' className="">
                <div className="clearfix">
                    <TextField
                        hintText="An Awsome text goes here."
                        floatingLabelText="Title"
                        value={this.state.title}
                        /*errorText="Title is required"*/
                        fullWidth={true}
                        floatingLabelFixed={true}
                        onChange={this.handleTextChange.bind(this)}
                    />
                </div>
                <div className="row clearfix">
                    <div className="col-sm-3">

                        <SelectField
                            floatingLabelFixed={true}
                            floatingLabelText="Pick your cause"
                            value={this.state.type}
                            onChange={this.handleChange}
                            fullWidth={true}
                        >
                            <MenuItem value={0} primaryText="Compassion" />
                            <MenuItem value={1} primaryText="Volunteers" />
                            <MenuItem value={2} primaryText="ignite" />
                        </SelectField>
                        <DatePicker
                            hintText="Date"
                            floatingLabelText={date}
                            mode="landscape"
                            floatingLabelFixed={true}
                            fullWidth={true}
                            onChange={this.handleDateChange}
                        />
                        <TextField
                            hintText={targetValue}
                            floatingLabelText="Target"
                            onChange={this.handleTargetChange}
                            fullWidth={true}
                            floatingLabelFixed={true}

                        />
                        <div  className={showLocation + " set-form"}>
                            <TextField
                                hintText={"Separate Email by ','"}
                                floatingLabelText="Invite by Email"
                                fullWidth={true}
                                floatingLabelFixed={true}
                                onChange={this.handleRecipientChange}
                            />
                            <TimePicker
                                floatingLabelText="Time Start"
                                floatingLabelFixed={true}
                                hintText="9:00"
                                autoOk={true}
                                onChange={this.handleTimeStart}
                            />
                            <TimePicker
                                floatingLabelText="Time End"
                                hintText="12:00"
                                floatingLabelFixed={true}
                                autoOk={true}
                                onChange={this.handleTimeEnd}
                            />
                        </div>

                    </div>
                    <div className="col-sm-9">
                        <Card className="row">
                            <Editor
                                editorClassName="editor-class-name"
                                onChange={this.onEditorStateChange}
                                //onContentStateChange={this.onEditorStateChange}
                            />
                            <Dropzone
                                onDrop={this.onDrop}
                                accept="image/*, video/*"
                                className="col-sm-11 dropzone-container"
                                disableClick={(!!this.images.length)}
                            >
                                { (!this.images.length) ? <div className="overlayStyle" >Drop files...</div> : this.getImages()}
                            </Dropzone>
                        </Card>
                    </div>
                </div>
                <div className="row clearfix">
                    <div className="col-sm-6">

                    </div>

                </div>
                <div className="row clearfix">
                    <div className="col-sm-6">

                    </div>
                    <div className="col-sm-6 text-right">
                        <FlatButton label="Cancel" />
                        <RaisedButton
                            onClick={this.handleSubmit}
                            label="Submit"
                            primary={true} style={{
                                margin: 12,
                        }} />
                    </div>
                </div>

            </div>
        );
    }

}
export default withRouter(CreatePost);
