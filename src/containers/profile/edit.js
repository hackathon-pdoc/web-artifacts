import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import './sidebar.css';
import ProfileSidebar from './sidebar.js';
import EditAddress from './editAddress.js';
import EditContact from './editContact.js';
import { invokeProfile } from '../../libs/awsLib';

class EditProfile extends Component {
    contact = {}
    address = {}
    constructor(props) {
        super(props);
        this.file = null;

        this.state = {
            firstName: "",
            lastName: "",
            contact: {},
            address: {}
        };
    }
    componentDidMount() {
        if(this.props.profile) {
            let profile = this.props.profile || {
                firstName: "",
                lastName: "",
            };
            this.setState({
                firstName: profile.firstName,
                lastName: profile.lastName,
            });
        }
    }

    handleSubmit = async (event) => {
        event.preventDefault();
        var body = {
            ...this.state,

        };

        console.log(body)

        return invokeProfile({path: '/profile', method: "POST", body: body}, this.props.userToken);
    }
    handleContactChange(field, value) {
        // this.setState({contact{[field]: value});
        this.setState({contact: {
            [field]:value
        }});

        console.log(this.state.contact)
    }
    handleAddressChange(field, value) {
        this.address[field] = value;
    }
    handleChange(field, e) {
        var nextState = {}
        nextState[field] = e.target.value;
        this.setState(nextState)
    }
    render() {
        console.debug(this.props);

        return (
            <div className="container edit-profile-container">
                <div className="row">
                    <div className="col-sm-3">
                        <ProfileSidebar {...this.props}/>
                    </div>
                    <div className="col-sm-9">
                        <div className="alert alert-info alert-dismissable">
                            <a className="panel-close close" data-dismiss="alert">×</a>
                            <i className="fa fa-coffee"></i>
                            This is an <strong>.alert</strong>. Use this to show important messages to the user.
                        </div>
                        <h3>Personal info</h3>
                        <form className="form-horizontal" onSubmit={this.handleSubmit} role="form-profile">
                            <div className="form-group">
                                <label className="col-md-3 control-label">First name:</label>
                                <div className="col-md-8">
                                    <input className="form-control" value={this.state.firstName} onChange={this.handleChange.bind(this, "firstName")}  type="text"/>
                                </div>
                            </div>
                            <div className="form-group">
                                <label className="col-md-3 control-label">Last Name:</label>
                                <div className="col-md-8">
                                    <input className="form-control" value={this.state.lastName} onChange={this.handleChange.bind(this, "lastName")} type="text"/>
                                </div>
                            </div>

                            <h3>Contact Details</h3>
                            <EditAddress handleChange={this.handleAddressChange} {...this.props}/>
                            <EditContact handleChange={this.handleContactChange.bind(this)} {...this.props}/>

                            <div className="form-group">
                                <label className="col-md-3 control-label"></label>
                                <div className="col-md-8">
                                    <input className="btn btn-primary" value="Save Changes" type="submit"/>
                                    <span></span>
                                    <input className="btn btn-default" value="Cancel" type="reset"/>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        );
    }
}

export default withRouter(EditProfile);
