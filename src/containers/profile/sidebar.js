import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

class ProfileSidebar extends Component {
    constructor(props) {
      super(props);
		console.debug("ProfileSidebar");
		console.log(props.profile);
      this.state = {
        isLoading: false,
        profile: props.profile.data,
      };
    }



  	render() {
    	return (
			<div className="profile-sidebar">
				<div className="profile-userpic">
					<div className="text-center">
						<img src="http://lorempixel.com/200/200/people/9/"
							 className="avatar img-circle img-thumbnail" alt="avatar"/>
						<h6>Upload a different photo...</h6>
						<input type="file" className="text-center center-block well well-sm"/>
					</div>
				</div>
				<div className="profile-usertitle">
					<div className="profile-usertitle-name">
						{this.state.profile.firstName} {this.state.profile.lastName}
					</div>
					<div className="profile-usertitle-job">
						Developer
					</div>
				</div>
				<div className="profile-userbuttons">
					<button type="button" className="btn btn-success btn-sm">Follow</button>
					<button type="button" className="btn btn-danger btn-sm">Message</button>
				</div>
				<div className="profile-usermenu">
					<ul className="nav">
						<li className="active">
							<a href="/profile/edit">
							<i className="glyphicon glyphicon-home"></i>
							Edit Profile </a>
						</li>
						<li>
							<a href="#test">
							<i className="glyphicon glyphicon-user"></i>
							Account Settings </a>
						</li>
						<li>
							<a href="#test">
							<i className="glyphicon glyphicon-ok"></i>
							Tasks </a>
						</li>
						<li>
							<a href="#test">
							<i className="glyphicon glyphicon-flag"></i>
							Help </a>
						</li>
					</ul>
				</div>
			</div>
		);
    }
}

export default withRouter(ProfileSidebar);
