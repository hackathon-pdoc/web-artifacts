import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import Divider from 'material-ui/Divider';
import RaisedButton from 'material-ui/RaisedButton';
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import ImageGallery from 'react-image-gallery';
import {invokeArticleAPI} from '../../libs/awsLib'
import LinearProgress from 'material-ui/LinearProgress';
import ActionForm from './ActionForm';
import ListByType from './ListByType';
import  './Details.css';
import moment from 'moment'
import Comments from './Comments'

class Details extends Component {
    images = [
        {
            original: 'http://lorempixel.com/1000/600/nature/1/',
            thumbnail: 'http://lorempixel.com/250/150/nature/1/',
        },
        {
            original: 'http://lorempixel.com/1000/600/nature/2/',
            thumbnail: 'http://lorempixel.com/250/150/nature/2/'
        },
        {
            original: 'http://lorempixel.com/1000/600/nature/3/',
            thumbnail: 'http://lorempixel.com/250/150/nature/3/'
        }
    ];
    static childContextTypes = {
        muiTheme: React.PropTypes.object
    }
    getChildContext() {
        return {
            muiTheme: getMuiTheme()
        }
    }
    submitForm() {
        this.actionForm.submitForm();
    }
    constructor(props) {
        super(props)
        console.debug(props);
        this.isLoggedId=false;
        this.state= {
            isLoading:false,
            article: [],
            comments:[]
        }
        this.submitForm = this.submitForm.bind(this);
        this.getArticles = this.getArticles.bind(this)
    }
    async getArticles() {
        const results = await this.articles(this.props.match.params.id);
        this.setState({ article: results[0], comments:results, isLoading: false  });
    }
    async articles(id) {
        let _this = this;
        let data = await invokeArticleAPI({path: '/article/'+id}, null);
        if(data.length===0) this.props.history.push('/');
        if(data[0].Banners.length!==0) this.images = [];
        JSON.parse(data[0].Banners).forEach((i)=> {
            _this.images.push({
                original: i.Location,
                thumbnail: i.Location
            });
        });


        return data;
    }
    async componentDidMount() {
        var _this = this;
        if (this.props.userToken !== null) {
            this.isLoggedId = true
        }

        this.setState({ isLoading: true });

        try {
            const results = await this.articles(this.props.match.params.id);
            console.log(results);

            this.setState({ article: results[0], comments:results, isLoading: false  });


        }
        catch(e) {
            console.error(e);
            this.setState({isLoading:false});
        }



    }
    createMarkup(data) {
        return {__html: data};
    }


    render() {
        if(this.state.article.length==0) return null;
        const {ArticleID, ArticleType, Title, Descr, Location,hasContributed, Count, Target, EventDate, ExpireDat, Banners, CreatorsEmail, CreatedByUser, CreatedDateTime, ModifiedDateTime} = this.state.article;
        var TotalContribution = Count,
            TotalContributionText = "",
            TotalContributionType = TotalContribution,
            buttonLabel = "Sign Up",
            willExpire=moment(EventDate, "YYYYMMDD").fromNow(),
            willExpires=moment(EventDate, "YYYYMMDD").fromNow().indexOf("in")<0,
            willExpireText="";
        switch (ArticleType) {
            case "VNT":
                TotalContributionText = "Current Volunteers";
                buttonLabel= "Volunteer";
                willExpireText=willExpires? "Has Expired " : "Will expire";
                break;
            case "CTR":
                TotalContributionText = "Total Contribution";
                TotalContributionType = "$ " + (Math.round(TotalContributionType*Math.pow(10,2))/Math.pow(10,2)).toFixed(2);
                buttonLabel= "Contribute";
                willExpireText=willExpires ? "Has Ended " : "Will End ";
                break;
            default:
                TotalContributionText = "Total Signatures";
                willExpireText=willExpires ? "Has Expired " : "Will expire";

        }


        return (
            <div id="create-post-container">
                <div className="row">
                    <div className="col-xs-6">

                        <ImageGallery
                            items={this.images}
                            slideInterval={2000}
                            showThumbnails={false}
                            showFullscreenButton={false}
                            showPlayButton={false}
                        />
                    </div>
                    <Card  className="col-xs-6">
                        <CardHeader
                            className="card-header-details-summary"
                            title={Title}
                            subtitle={CreatorsEmail}
                        />

                        <CardText>
                            <div className="details-data">
                                <p>{willExpireText}</p>
                                <h3>{willExpire}</h3>
                            </div>
                            <h4>{TotalContributionText} <strong style={{float: "right"}}>{TotalContributionType}</strong></h4>
                            <LinearProgress mode="determinate" value={TotalContribution} max={Target}/>
                        </CardText>
                        <CardActions>

                        </CardActions>
                    </Card>

                </div>
                <div className="row">
                    <div className="col-xs-9">
                        <div dangerouslySetInnerHTML={this.createMarkup(Descr)} ></div>
                    </div>
                    <div className="col-xs-3 row">
                        <Card className="side-card row">
                            <CardText className="row">
                                <ActionForm  data={this.state.article} refresh={this.getArticles}  ArticleId={ArticleID} ref={instance => { this.actionForm = instance; }} {...this.props} ArticleType={ArticleType}/>
                            </CardText>
                            <CardActions className="col-sm-12 clearfix">
                                <div className="col-sm-12">
                                    <RaisedButton
                                        primary={true}
                                        onClick={this.submitForm}
                                        fullWidth={true}
                                        label={buttonLabel}
                                    />
                                </div>
                                <div className="col-sm-12 clearfix">
                                    <FlatButton
                                        label="Add to Calendar"
                                        fullWidth={true}
                                        icon={<span className="glyphicon glyphicon-calendar" />}
                                    />
                                </div>
                            </CardActions>
                        </Card>
                        {/*(ArticleType==="VNT") ?
                        <Card className="side-card row">
                            <CardText className="row">
                                <ActionForm {...this.props} ArticleType={ArticleType} ArticleId={ArticleID}/>
                            </CardText>
                            <CardActions className="col-sm-12 clearfix">
                                <div className="col-sm-12">
                                    <RaisedButton
                                        primary={true}
                                        fullWidth={true}
                                        label={buttonLabel}
                                    />
                                </div>
                                <div className="col-sm-12 clearfix">
                                    <FlatButton
                                        label="Add to Calendar"
                                        fullWidth={true}
                                        icon={<span className="glyphicon glyphicon-calendar" />}
                                    />
                                </div>
                            </CardActions>
                        </Card> : null*/}

                    </div>
                </div>
                <div className="row">
                    <h3>Comments</h3>
                    <Divider />
                </div>
                    <Comments refresh={this.getArticles} {...this.props} comments={this.state.comments} ArticleId={ArticleID}/>
                <div className="row">
                    <h3>Similar Campaigns:</h3>
                    <Divider />

                </div>
                <div className="row view-similar">
                    <ListByType className="" articleType={ArticleType}/>
                </div>
            </div>
        );
    }
}
export default withRouter(Details);
