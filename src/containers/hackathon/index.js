import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import './index.css';
import {
    Jumbotron,
} from 'react-bootstrap';
import Carousel from './Carousel';
import {invokeArticleAPI} from '../../libs/awsLib'

class Hackathon extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            articles: [],
        };
    }
    async articles() {
        let data = await invokeArticleAPI({}, null);

        return data;
    }
    async componentDidMount() {
        this.setState({ isLoading: true });

        try {
            const results = await this.articles();
            this.setState({ articles: results });
        }
        catch(e) {
            // alert(e);
        }

        this.setState({ isLoading: false });
    }
    render() {
        return (
            <div className="profile">
                <Jumbotron bsClass="jumbotron clearfix" >

                    <h1>Hello, world!</h1>
                    <p className="col-sm-5">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
                    <img src="http://i.dailymail.co.uk/i/pix/2013/11/12/article-2501471-195CE8C000000578-51_964x634.jpg" alt="" className="img"/>
                    <div className="gradient"></div>
                </Jumbotron>
                {/*
                    'CTR', //Contribute/Donate?
                    'VNT', //Volunteer
                    'SGT' //Suggestion
                */}
                <Carousel {...this.props} title="Volunteer Needed" type="VNT" articleData={this.state.articles}/>
                <Carousel {...this.props} title="Donation Needed" type="CTR" articleData={this.state.articles}/>
                <Carousel {...this.props} title="Change Needed" type="SGT" articleData={this.state.articles}/>
            </div>
        );
    }
}

export default withRouter(Hackathon);
