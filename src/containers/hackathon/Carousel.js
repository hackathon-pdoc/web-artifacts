import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import './Carousel.css';
import Slider  from 'react-slick';
import Cards  from './Cards';
import {
    Jumbotron,
} from 'react-bootstrap';

String.prototype.trunc =
    function( n, useWordBoundary ){
        if (this.length <= n) { return this; }
        var subString = this.substr(0, n-1);
        return (useWordBoundary
            ? subString.substr(0, subString.lastIndexOf(' '))
            : subString) + "...";
    };
class Carousel extends Component {
    banners=[];
    constructor(props) {
        super(props);
        this.state = {
            showDetails: 'hidden-details',
            num: 0,
            detailTitle: '',
            detailDescription: '',
            banner: ''
        };

        this.toggleDetails = this.toggleDetails.bind(this);
        this.next = this.next.bind(this);
        this.previous = this.previous.bind(this);
        this.prevButton = this.prevButton.bind(this);
        this.nextButton = this.nextButton.bind(this)
        this.previous = this.previous.bind(this);
    }
    next() {
        this.slider.slickNext()
    }
    previous() {
        this.slider.slickPrev()
    }
    toggleDetails(index) {
        if(index !== this.state.num && this.state.showDetails === 'shown-details') {
            this.setState({
                num: index,
                detailTitle: this.props.articleData[index].Title,
                detailDescription: this.props.articleData[index].Descr.trunc(150,true),
                banner: this.banners[index][0].Location
            });
            return;
        }
        this.setState({
            showDetails: (this.state.showDetails !== 'hidden-details') ? "hidden-details" : "shown-details",
            num: index,
        });
    }
    prevButton() {
        return (
            <div className="slick-prev slick-navigation" onClick={this.previous}>
                <span className="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            </div>
        )
    }
    nextButton() {
        return (
            <div  className="slick-next slick-navigation" onClick={this.next}>
                <span className="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            </div>
        )
    }

    loadCards() {
        let arCards=[], _this = this;

        this.props.articleData.forEach((obj, i)=> {
            if(_this.props.type===obj.ArticleType) {
                _this.banners[i] = JSON.parse(obj.Banners)
                arCards.push(
                    <div className="col-sm-2 box-slider" key={obj.ArticleID}>
                        <Cards {..._this.props} article={obj} />
                    </div>

                );
            }
        });
        return arCards;
    }

    render() {
        var settings = {
            dots: false,
            infinite: false,
            speed: 500,
            slidesToShow: 4,
            slidesToScroll: 10,
            initialSlide: 0,
            swipeToSlide:false,
            /*prevArrow: <this.prevButton />,
            nextArrow: <this.nextButton />
            responsive: [{
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            }, {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    initialSlide: 2
                }
            }, {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }]*/
        };
        return (
            <div>
                <h3>{this.props.title}</h3>
                <Slider ref={c => this.slider = c } {...settings}>
                    {this.loadCards()}
                </Slider>
                <Jumbotron bsClass={this.state.showDetails + " jumbotron clearfix"}>
                    <h1>{this.state.detailTitle}</h1>
                    <p className="col-sm-5">{this.state.detailDescription}</p>
                    <img src={this.state.banner} alt="" className="img"/>
                    <div className="gradient"></div>
                </Jumbotron>
            </div>
        );
    }
}

export default withRouter(Carousel);
