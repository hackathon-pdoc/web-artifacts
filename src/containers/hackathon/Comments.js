import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import FlatButton from 'material-ui/FlatButton';
import Divider from 'material-ui/Divider';
import RaisedButton from 'material-ui/RaisedButton';
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import TextField from 'material-ui/TextField';
import {invokeContributeAPI} from '../../libs/awsLib'
import moment from 'moment'


class Comments extends Component {

    static childContextTypes = {
        muiTheme: React.PropTypes.object
    }
    getChildContext() {
        return {
            muiTheme: getMuiTheme()
        }
    }

    constructor(props) {
        super(props);
        console.debug(props);
        this.state= {
            Description:''
        };
        this.submitForm = this.submitForm.bind(this);
        this.handleChange = this.handleChange.bind(this);

    }
    handleChange = (event) => {
        this.setState({
            [event.target.id]: event.target.value
        });
    }
    comments() {
        if(this.props.comments.length===0) return null;
        return this.props.comments.map((data) => {
            console.debug(data);
            return (
                <Card key={data.CommentID} className="side-card row">
                    <CardHeader
                        title={data.email}
                        subtitle={data.CommentDate}
                        avatar="images/jsa-128.jpg"
                    />
                    <CardText>
                        {data.Description}
                    </CardText>
                </Card>
            )
        });
    }
    async submitForm() {
        let payload = {
            ArticleID: this.props.ArticleId,
            Description: this.state.Description
        }, data;

        try {
            let data = await invokeContributeAPI({body: payload, method: "PUT", path: '/comment'},  this.props.userToken);
            console.log(data);
            if(data.insertId) {
                await this.props.refresh();
            };
        } catch (e) {

        }
        console.debug(data);
    }
    render() {
        return (
            <div className="col-sm-12">
                <TextField
                    floatingLabelText="Comment:"
                    id="Description"
                    floatingLabelFixed={true}
                    fullWidth={true}
                    onChange={this.handleChange}
                />

                <Divider/>
                <div className="side-card row">
                    <RaisedButton
                        onClick={this.submitForm}
                        primary={true}
                        fullWidth={true}
                        label="Submit"
                    />
                </div>
                {this.comments()}
            </div>

        );
    }
}
export default withRouter(Comments);
