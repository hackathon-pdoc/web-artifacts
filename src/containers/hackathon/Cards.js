import React, { Component } from 'react';
import {Card, CardActions, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import LinearProgress from 'material-ui/LinearProgress';
// import FlatButton from 'material-ui/FlatButton';
import getMuiTheme from 'material-ui/styles/getMuiTheme'

String.prototype.trunc =
    function( n, useWordBoundary ){
        if (this.length <= n) { return this; }
        var subString = this.substr(0, n-1);
        return (useWordBoundary
            ? subString.substr(0, subString.lastIndexOf(' '))
            : subString) + "...";
    };
export default class Cards extends Component {
    constructor(props) {
        super(props)
        this.banners = JSON.parse(props.article.Banners)
        this.banner = this.banners[Math.floor(Math.random() * this.banners.length)].Location;
        this.navigate = this.navigate.bind(this)
    }
    static childContextTypes = {
        muiTheme: React.PropTypes.object
    }
    getChildContext() {
        return {
            muiTheme: getMuiTheme()
        }
    }
    navigate() {
        this.props.history.push('/article/'+this.props.article.ArticleID, {...this.props.article})
    }
    strip(html)
    {
        var tmp = document.createElement("DIV");
        tmp.innerHTML = html;
        return tmp.textContent || tmp.innerText || "";
    }
    overlay() {
        return (
            <div className="overlay">
                <h5>Target:</h5>
                <LinearProgress mode="determinate" value={this.props.article.TotalContribution} max={this.props.article.Target}/>
            </div>
        )
    }
    render() {
        return (
            <Card>
                {/*<CardHeader
                    title="URL Avatar"
                    subtitle="Subtitle"

                />*/}
                <CardMedia
                    overlay={this.overlay()}
                >
                    <img src={this.banner} alt="none"/>
                </CardMedia>
                <CardTitle className="card-title" title={this.props.article.Title} subtitle={this.props.article.CreatedByUser}/>
                <CardText className="content-container">
                    {this.strip(this.props.article.Descr).trunc(100, true)}
                </CardText>
                <CardActions>
                    <RaisedButton onClick={this.navigate} label="Read More" primary={true}/>
                </CardActions>
            </Card>
        );
    }
}