import React, { Component } from 'react';
import FlatButton from 'material-ui/FlatButton';
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import TextField from 'material-ui/TextField';
import {invokeContributeAPI} from "../../libs/awsLib";


export default class ListByType extends Component {

    handleChange = (event) => {
        this.setState({
            [event.target.id]: event.target.value
        });
    }
    async submitForm() {
        var payload = {
            UserID: this.state.email,
            ArticleID: this.props.ArticleId,
        }, data;
        switch (this.props.ArticleType) {
            case 'CTR':
                payload= {
                    ...payload,
                    Amount: this.state.amount,
                    ContributionType: 'SAL',
                };
                console.debug(payload);
                data = await invokeContributeAPI({body: payload, method: "PUT", path: '/contribute'},  this.props.userToken);
                break;
            case 'SGT':
                data = await invokeContributeAPI({body: payload, method: "PUT", path: '/vote'},  this.props.userToken);
                break;
            default:
                data = await invokeContributeAPI({body: payload, method: "PUT", path: '/volunteer'},  this.props.userToken);
                break;
        }
        if(data.insertId) {
            await this.props.refresh();
        };
        console.debug(payload);
    }
    constructor(props) {
        super(props)
        this.isLoggedId=false;
        this.state= {
            isLoading:true,
            comment: "",
            email: props.profile.email || "",
            amount: 1
        };
        this.handleChange = this.handleChange.bind(this);
        this.submitForm = this.submitForm.bind(this);
    }

    componentDidMount() {

    }

    renderVNT() {
        return(
            <div className="col-md-12">
                <TextField
                    fullWidth={true}
                    id="email"
                    value={this.state.email}
                    hintText="juan@example.com"
                    floatingLabelText="Email"
                    floatingLabelFixed={true}
                    onChange={this.handleChange}
                />

            </div>
        )
    }
    renderSGT() {
        return(
            <div className="col-md-12">
                <TextField
                    hintText="juan@example.com"
                    id="email"
                    value={this.state.email}
                    fullWidth={true}
                    floatingLabelText="Email"
                    floatingLabelFixed={true}
                    onChange={this.handleChange}
                />

            </div>
        )
    }
    renderCTR() {
        return(
            <div className="col-md-12">
                <TextField
                    hintText="500.00"
                    floatingLabelText="Amount"
                    id="amount"
                    floatingLabelFixed={true}
                    fullWidth={true}
                    onChange={this.handleChange}
                />
                <TextField
                    hintText="juan@example.com"
                    value={this.state.email}
                    id="email"
                    floatingLabelText="Email"
                    floatingLabelFixed={true}
                    fullWidth={true}
                    onChange={this.handleChange}
                />
            </div>
        )
    }
    search(nameKey, myArray){
        console.debug(arguments);
        for (var i=0; i < myArray.length; i++) {
            if (myArray[i].UserID === nameKey) {
                return true;
            }
        }
        return false;
    }
    render() {
        var showForm=null;
        switch (this.props.ArticleType) {
            case "VNT":
                showForm=this.renderVNT();
                break;
            case "CTR":
                showForm=this.renderCTR();

                break;
            default:
                showForm=this.renderSGT();
        }
        if(this.search(this.props.profile._id, this.props.data)) return (<div>You have been a contributor</div>);
        return showForm;
    }
}
